"""sopra URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from sopraControl import views as views_control
from accounts import views as views_acounts
from django.contrib import admin
from django.urls import path


urlpatterns = [
    path('admin/', admin.site.urls),
    path('', views_control.index, name='index'),
    path('sopra/', views_control.sopra, name='sp_control'),
    path('search/', views_control.search),
    path('searching/', views_control.searching),
    path('log/', views_acounts.log_sopra, name='login'),
    path('logout/', views_acounts.logout_view, name='logout'),
    path('devices/', views_control.devices)
]
