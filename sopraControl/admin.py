from django.contrib import admin
from sopraControl.models import Users, Devices, Hour


class DevicesAdmin(admin.ModelAdmin):
    list_display = ("uid", "state")


class HourAdmin(admin.ModelAdmin):
    list_display = ("timeOn", "timeOff")


admin.site.register(Users)
admin.site.register(Devices, DevicesAdmin)
admin.site.register(Hour, HourAdmin)

# Register your models here.
