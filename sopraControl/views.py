from django.http import HttpResponse
from django.shortcuts import render
from .models import Users, Devices
from django.contrib.auth.decorators import login_required
from django.contrib.auth import get_user_model


def index(request):
    context = {
        "title": 'Bienvenido',
    }
    return render(request, 'index.html', context)


@login_required
def sopra(request):
    vec = []
    if request.user.is_authenticated:
        user_mail = request.user.email
        print(user_mail)
        user_id = Users.objects.filter(mail=user_mail)
        uid = user_id[0].uid
        dev_user = Devices.objects.filter(uid=uid)
        for i in dev_user:
            vec.append(i.name_device)
        print(dev_user)
    else:
        print('chao')
    return render(request, 'sopra.html', context={'devices': vec})


def devices(request):
    if request.user.is_authenticated:
        if request.GET['device']:
            user_mail = request.user.email
            user_id = Users.objects.filter(mail=user_mail)
            uid = user_id[0].uid
            n_dv = request.GET['device']
            dev_user = Devices.objects.filter(uid=uid, name_device=n_dv)
            dv_state = dev_user[0].state
            if dv_state: state = "checked"
            else: state = ""
        elif request['checked']:
            c_state = request.GET['checked']
            print(c_state)
        return render(request, "device.html", context={'n_device': n_dv, 'state':state})

    else: return render(request, 'log.html')



def search(request):
    return render(request, 'search.html')


def searching(request):
    # msg = request.GET["user"]
    # return HttpResponse(msg)
    if request.GET['user']:
        print(True)
        b_user = request.GET['user']
        v_user = Users.objects.filter(name__icontains='Esteban')
        return render(request, "resultado.html", {"Usuarios":v_user, "Usuario":b_user})
    else: return HttpResponse("Introduzca un nombre")
