from django.db import models


class Users(models.Model):
    name = models.CharField(max_length=30)
    mail = models.EmailField()
    phone = models.CharField(max_length=10)
    uid = models.CharField(max_length=10, default="")

    def __str__(self):
        return self.mail


class Devices(models.Model):
    uid = models.CharField(max_length=10)
    state = models.BooleanField()
    name_device = models.CharField(max_length=30)

    def __str__(self):
        return self.name_device


class Hour(models.Model):
    timeOn = models.DateField()
    timeOff = models.DateField()
    uid = models.CharField(max_length=10, default="")

    def __str__(self):
        return self.uid