from django.apps import AppConfig


class SopracontrolConfig(AppConfig):
    name = 'sopraControl'
